use crossterm::{
    event::{poll, read, Event, KeyCode},
    terminal::{disable_raw_mode, enable_raw_mode, size},
};
use rand::prelude::*;
use std::collections::VecDeque;
use std::time::{Duration, Instant};

const FRAME_TIME: Duration = Duration::from_millis(100);
const SNAKE_COLOR: Color = Color { r: 0, g: 255, b: 0 };
const APPLE_COLOR: Color = Color { r: 255, g: 0, b: 0 };

#[derive(PartialEq)]
enum Direction {
    Up,
    Down,
    Left,
    Right,
}

enum CrashLocation {
    Snake,
    North,
    South,
    East,
    West,
}

fn main() {
    let mut snake: VecDeque<(u16, u16)> = vec![(0, 0), (1, 0), (2, 0)].into();
    let mut direction = Direction::Right;
    let mut length = snake.len();
    let (width, height) = size().unwrap_or((20, 20));
    let (width, height) = ((width - 2) / 2, height - 5);

    let mut rng = thread_rng();
    let mut apple = (rng.gen_range(0..width), rng.gen_range(0..height));

    print!("\x1b[?25l"); // Hides cursor

    loop {
        let now = Instant::now();

        render(&snake, apple, length, (width, height));
        enable_raw_mode().unwrap();
        change_dir(&mut direction, &now);
        disable_raw_mode().unwrap();
        if let Some(crash) = move_snake(&mut snake, &direction, length, (width, height)) {
            match crash {
                CrashLocation::North => println!("You crashed in the North wall!"),
                CrashLocation::South => println!("You crashed in the South wall!"),
                CrashLocation::East => println!("You crashed in the East wall!"),
                CrashLocation::West => println!("You crashed in the West wall!"),
                CrashLocation::Snake => println!("You crashed in yourself!"),
            }
            break;
        }
        eat(&snake, &mut apple, &mut length, &mut rng, (width, height));
    }

    exit();
}

fn eat(
    snake: &VecDeque<(u16, u16)>,
    apple: &mut (u16, u16),
    length: &mut usize,
    rng: &mut ThreadRng,
    (width, height): (u16, u16),
) {
    if snake.contains(apple) {
        *apple = (rng.gen_range(0..width), rng.gen_range(0..height));
        *length += 1;
    }
}

fn exit() {
    disable_raw_mode().unwrap();
    print!("\x1b[?25h"); // Shows cursor
    std::process::exit(0);
}

fn change_dir(direction: &mut Direction, now: &Instant) {
    if let Some(k) = key_input(now) {
        match k {
            'd' => {
                if *direction != Direction::Left {
                    *direction = Direction::Right;
                }
            }
            'a' => {
                if *direction != Direction::Right {
                    *direction = Direction::Left;
                }
            }
            'w' => {
                if *direction != Direction::Down {
                    *direction = Direction::Up;
                }
            }
            's' => {
                if *direction != Direction::Up {
                    *direction = Direction::Down;
                }
            }
            'q' => exit(),
            _ => (),
        }
    }
}

fn key_input(now: &Instant) -> Option<char> {
    while now.elapsed() < FRAME_TIME {
        if poll(Duration::from_secs(0)).ok()? {
            if let Event::Key(k) = read().ok()? {
                if let KeyCode::Char(c) = k.code {
                    return Some(c);
                }
            }
        }
    }
    None
}

fn move_snake(
    snake: &mut VecDeque<(u16, u16)>,
    direction: &Direction,
    length: usize,
    (width, height): (u16, u16),
) -> Option<CrashLocation> {
    if length == snake.len() {
        snake.pop_front();
    }
    let mut a = *snake.iter().last().unwrap();

    match direction {
        Direction::Right => {
            if a.0 + 1 < width {
                a.0 += 1
            } else {
                return Some(CrashLocation::East);
            }
        }
        Direction::Left => {
            if a.0 > 0 {
                a.0 -= 1
            } else {
                return Some(CrashLocation::West);
            }
        }
        Direction::Up => {
            if a.1 + 1 < height {
                a.1 += 1
            } else {
                return Some(CrashLocation::North);
            }
        }
        Direction::Down => {
            if a.1 > 0 {
                a.1 -= 1
            } else {
                return Some(CrashLocation::South);
            }
        }
    }

    if snake.contains(&a) {
        return Some(CrashLocation::Snake);
    }

    snake.push_back(a);
    None
}

fn render(
    snake: &VecDeque<(u16, u16)>,
    apple: (u16, u16),
    length: usize,
    (width, height): (u16, u16),
) {
    let mut s = String::new();
    s.push_str("\u{001b}c");
    s.push_str(&format!("length: {length}\n"));
    s.push('┏');
    s.push_str(&"━".repeat(width as usize * 2));
    s.push_str("┓\n");
    for y in (0..height).rev() {
        s.push('┃');
        for x in 0..width {
            if snake.contains(&(x, y)) {
                s.push_str(&"  ".bg(SNAKE_COLOR));
            } else if (x, y) == apple {
                s.push_str(&"  ".bg(APPLE_COLOR));
            } else {
                s.push_str("  ");
            }
        }
        s.push_str("┃\n");
    }
    s.push('┗');
    s.push_str(&"━".repeat(width as usize * 2));
    s.push('┛');
    println!("{s}");
}

struct Color {
    r: u8,
    g: u8,
    b: u8,
}

trait Colorize {
    fn bg(&self, color: Color) -> String;
}

impl<T: std::fmt::Display> Colorize for T {
    fn bg(&self, color: Color) -> String {
        format!(
            "\x1b[48;2;{};{};{}m{self}\x1b[0m",
            color.r, color.g, color.b
        )
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn move_test() {
        let mut snake: VecDeque<(u16, u16)> = vec![(16, 16), (17, 16), (18, 16)].into();
        let mut direction = Direction::Up;
        let length = snake.len();
        let size = size().unwrap_or((20, 20));
        move_snake(&mut snake, &direction, length, size);
        assert_eq!(snake, vec![(17, 16), (18, 16), (18, 17)]);

        direction = Direction::Right;
        move_snake(&mut snake, &direction, length, size);
        assert_eq!(snake, vec![(18, 16), (18, 17), (19, 17)]);

        direction = Direction::Down;
        move_snake(&mut snake, &direction, length, size);
        assert_eq!(snake, vec![(18, 17), (19, 17), (19, 16)]);

        direction = Direction::Left;
        move_snake(&mut snake, &direction, length, size);
        assert_eq!(snake, vec![(19, 17), (19, 16), (18, 16)]);
    }
}
